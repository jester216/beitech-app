export class Order{
    constructor(
        public order_id: number,
        public customer_id: number,
        public creation_date: string,
        public delivery_address: string,
        public total: number
    ){}
}