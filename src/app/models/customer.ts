export class Customer{
    constructor(
        public customer_id: number,
        public name: string,
        public email: string
    ){}
}