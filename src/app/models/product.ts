export class Product{
    constructor(
        public product_it: number,
        public name : string,
        public product_description: string,
        public price: number
    ){}
}