export class Order_detail{
    constructor(
        public order_detail_id: number,
        public order_id: number,
        public product_id: number,
        public product_description: string,
        public price: number,
        public quantity: number,
        public total_price: number,
        public product_name: string

    ){}
}