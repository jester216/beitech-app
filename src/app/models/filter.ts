export class Filter{
    constructor(
        public customer_id: number,
        public start: string,
        public end: string
    ){}
}