import { Component, OnInit } from '@angular/core';
import { TableService } from '../../services/table.service';
import { Filter } from '../../models/filter';
import { FilterService } from '../../services/filter.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [TableService,FilterService]
})
export class TableComponent implements OnInit {
  public filter : Filter;
  public options;
  public data;
  constructor(
    private _tableService : TableService,
    private _filterService: FilterService
  ) {
    this.filter = new Filter(0,'','');
   }

  ngOnInit(): void {
    this.populateTable();
    this.getCustomers();
  }

  onSubmit(){
    this.populateTable();
  }

  getCustomers(){
    this._filterService.fillSelect().subscribe(
      response => {
        if(response.status == true){
          this.options = response.data;

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }

  populateTable(){
    this._tableService.populate(this.filter).subscribe(
      response => {
        if(response.status == true){
          this.data = response.data;
          

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }
}
