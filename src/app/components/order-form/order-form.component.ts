import { Component, OnInit } from '@angular/core';
import { Order } from '../../models/order';
import { FilterService } from '../../services/filter.service';
import { OrderService } from '../../services/order.service';
import { Order_detail } from 'src/app/models/order_detail';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css'],
  providers: [FilterService,OrderService]
})
export class OrderFormComponent implements OnInit {

  public order : Order;
  public orderDetail : Order_detail;
  public orderDetail2 : Order_detail;
  public options;
  public products;
  public productDetail;
  public selectedProducts: Order_detail[];
  public disableCustomer = false;
  public form;

  constructor(
    private _filterService: FilterService,
    private _orderService: OrderService
  ) {
    this.order = new Order(0,0,'','',0);    
    this.orderDetail = new Order_detail(0,0,0,'',0,0,0,'');
    
  }

  ngOnInit(): void {
    this.getCustomers();
  }

  /**
   * Form events
   */
  
  onChangeCustomer(customer){
    let id = customer.viewModel
    this.getProducts(id);
    this.disableCustomer = true;
  }
  
  onChangeProduct(product){
    let id = product.viewModel
    this.getProductDetail(id);
    this.orderDetail.quantity = 0;
  }
  onSubmit(form){
    this.form = form;
    console.log(this.order);
    console.log(this.selectedProducts);
    this.register();
    
  }
  addProduct(quantity){
    this.orderDetail2 = new Order_detail(0,0,0,'',0,0,0,'');
    this.orderDetail2.price = this.productDetail.price;
    this.orderDetail2.product_description = this.productDetail.product_description;
    this.orderDetail2.product_id = this.productDetail.id;
    this.orderDetail2.product_name = this.productDetail.name;
    this.orderDetail2.total_price = (this.orderDetail2.price * quantity);
    this.orderDetail2.quantity = (this.orderDetail2.total_price / this.orderDetail2.price);
    this.order.total += this.orderDetail2.total_price;
    if(this.selectedProducts === undefined){
      this.selectedProducts = [this.orderDetail2]
    }else{
      this.selectedProducts.push(this.orderDetail2);
    }

  }

  /**
   * Server request handled
   */

  getCustomers(){
    this._filterService.fillSelect().subscribe(
      response => {
        if(response.status == true){
          this.options = response.data;

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }
  
  getProducts(id){
    
    this._filterService.fillProducts(id).subscribe(
      response => {
        if(response.status == true){
          this.products = response.data;

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }
  
  getProductDetail(id){
    
    this._filterService.productDetail(id).subscribe(
      response => {
        if(response.status == true){
          this.productDetail = response.data;

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }
  register(){
    
    this._orderService.register(this.order,this.selectedProducts).subscribe(
      response => {
        if(response.status == true){
          alert(response.message)
          this.closeForm() 
        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }
  closeForm(){
    this.form.reset();
    this.disableCustomer = false;
    this.selectedProducts.splice(0,this.selectedProducts.length);
    this.order.total = 0;
  }


}
