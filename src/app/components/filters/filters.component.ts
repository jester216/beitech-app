import { Component, OnInit } from '@angular/core';
import { Filter } from '../../models/filter';
import { FilterService } from '../../services/filter.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
  providers: [FilterService]
})
export class FiltersComponent implements OnInit {
  public filter : Filter;
  public options;
  public key: number;
  public option;
  constructor(
    private _filterService: FilterService
    ) {

      this.filter = new Filter(0,'','');
    }

  ngOnInit(): void {
    this.getCustomers();
  }

  onSubmit(form){
    console.log(this.filter)
  }

  getCustomers(){
    this._filterService.fillSelect().subscribe(
      response => {
        if(response.status == true){
          this.options = response.data;

        }
      },
      error => {
        console.log(<any>error);
      }
    )
  }

}
