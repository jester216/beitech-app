import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http' ;
import { Observable } from 'rxjs';
import { Filter } from '../models/filter';
import { global } from './global';

@Injectable()
export class FilterService {

    public url: string;

    constructor(
        public _http: HttpClient
    ){
        this.url = global.url;
    }


    fillSelect(): Observable<any>{
        let url = this.url + 'customers_list';
        let json = JSON.stringify({});
        let params = 'json='+json;

        let headers = new HttpHeaders().set('Contet-Type','application/x-www-form-urlencoded');
     

        return this._http.get(url)
    }
    
    fillProducts(id): Observable<any>{        
        let url = this.url + 'products_by_customer/'+id;
        let json = JSON.stringify({});
        let params = 'json='+json;

        let headers = new HttpHeaders().set('Contet-Type','application/x-www-form-urlencoded');
     

        return this._http.get(url)
    }
    
    productDetail(id): Observable<any>{        
        let url = this.url + 'products_detail/'+id;
        let json = JSON.stringify({});
        let params = 'json='+json;

        let headers = new HttpHeaders().set('Contet-Type','application/x-www-form-urlencoded');
     

        return this._http.get(url)
    }
}