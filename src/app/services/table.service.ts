import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http' ;
import { Observable } from 'rxjs';
import { Filter } from '../models/filter';
import { global } from './global';

@Injectable()
export class TableService {

    public url: string;

    constructor(
        public _http: HttpClient
    ){
        this.url = global.url;
    }


    populate(filter): Observable<any>{
        let url = this.url + 'orders';
        
        let params = new HttpParams().set("customer",filter.customer_id).set("start", filter.start).set("end", filter.end);

        let headers = new HttpHeaders().set('Contet-Type','application/x-www-form-urlencoded');
        //let headers = new HttpHeaders().set("Access-Control-Allow-Origin", "*");


        return this._http.get(url,{headers:headers, params: params})
    }
}