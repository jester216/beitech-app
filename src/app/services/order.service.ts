import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http' ;
import { Observable } from 'rxjs';
import { Order } from '../models/order';
import { Order_detail } from '../models/order_detail';
import { global } from './global';

@Injectable()
export class OrderService {

    public url: string;

    constructor(
        public _http: HttpClient
    ){
        this.url = global.url;
    }


    register(order,products): Observable<any>{
        let url = this.url + 'orders';
        //JSON.stringify(order)
        let jsonOrder = {jsonOrder:order, products:products};
        let params = 'jsonOrder='+JSON.stringify(jsonOrder);

        let headers = new HttpHeaders().set('Contet-Type','application/x-www-form-urlencoded');
     

        return this._http.post(url, jsonOrder, {headers: headers});
    }
    
    
}